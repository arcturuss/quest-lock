/*
 * GccApplication1.c
 *
 * Created: 10.07.2016 11:32:58
 * Author : Anton
 */

#define F_CPU 1000000UL

#include <avr/io.h>
#include <avr/eeprom.h>
#include <util/delay.h>
#include <stdlib.h>

#define _CONCAT(a,b) a##b
#define PORT(x) _CONCAT(PORT,x)
#define PIN(x) _CONCAT(PIN,x)
#define DDR(x) _CONCAT(DDR,x)

#define KEYPAD B

#define KEYPAD_PORT PORT(KEYPAD)
#define KEYPAD_DDR	DDR(KEYPAD)
#define KEYPAD_PIN	PIN(KEYPAD)

#define LED D

#define LED_PORT PORT(LED)
#define LED_DDR  DDR(LED)
#define LED_PIN  PIN(LED)

#define RED_LED PD0
#define GREEN_LED PD1
#define LOCK PD2

#define EEPROM_CHARS 2U
#define EEPROM_PASSWORD_LEN EEPROM_CHARS+12
#define EEPROM_PASSWORD EEPROM_PASSWORD_LEN+1

#define UPDATING_PASSWORD 0x01
#define LOCK_IS_OPEN 0x02

#define MAX_PASSWORD_LEN 70

char keyToChar(uint8_t key);
uint8_t passwordLength();
void handleKeyRead(const char key);
void initEEPROM();
void openLock();
void closeLock();
void greenLedOn();
void greenLedOff();
void redLedOn();
void redLedOff();
uint8_t getKeyPressed();

uint16_t currentChar_g = 0;
uint8_t flags_g = 0;
char* newPassword = NULL;

int main() {
	initEEPROM();

	LED_DDR |= (1 << DDD0) | (1 << DDD1) | (1 << DDD2);
	KEYPAD_DDR |= (0 << DDB0) | (0 << DDB1) | (0 << DDB2) | (0 << DDB3) | (1 << DDB4) | (1 << DDB5) | (1 << DDB6);
	flags_g &= ~UPDATING_PASSWORD;
	flags_g &= ~LOCK_IS_OPEN;
	closeLock();

	redLedOn();
	greenLedOff();

	uint8_t prev_key  = 0xFF; //'f';
	uint8_t curr_key  = 0xFF; //'f';
	uint8_t curr_key2 = 0xFF;

	_delay_ms(500);
	redLedOff();
	_delay_ms(500);
	redLedOn();
	_delay_ms(500);
	redLedOff();
	_delay_ms(500);
	redLedOn();
	_delay_ms(500);
	redLedOff();
	_delay_ms(500);
	redLedOn();
	// _delay_ms(1000);

	while(1) {
		_delay_ms(10);
		curr_key2 = getKeyPressed(); // ����������� ����
		_delay_ms(10);
		curr_key = getKeyPressed();
        if (curr_key == curr_key2) {
            if ((curr_key != 0xFF) && (curr_key != prev_key))
                {
                    handleKeyRead(keyToChar(curr_key));
                }
            prev_key = curr_key;
        }
	}
}

void handleKeyRead(const char key) {
	switch (key) {
		case '*':
			if ( !(flags_g & UPDATING_PASSWORD) ) {
				closeLock();
				// flags_g &= ~LOCK_IS_OPEN;
				greenLedOff();
				redLedOn();
				currentChar_g = 0;
			}
			if ( (flags_g & UPDATING_PASSWORD) && (newPassword != NULL )) {
				free(newPassword);
				newPassword = NULL;
				greenLedOff();
				redLedOn();
				_delay_ms(200);
				greenLedOn();
				redLedOff();
				currentChar_g = 0;
			}
			
			flags_g &= ~UPDATING_PASSWORD;
			break;

		case '#':
			if ( !(flags_g & UPDATING_PASSWORD) && (flags_g & LOCK_IS_OPEN) ) {
				flags_g |= UPDATING_PASSWORD;
				newPassword = malloc(MAX_PASSWORD_LEN);
				currentChar_g = 0;
				greenLedOff();
				_delay_ms(200);
				greenLedOn();
				_delay_ms(200);
				greenLedOff();		
				_delay_ms(200);
				greenLedOn();	
			}
			if ( (flags_g & UPDATING_PASSWORD) && (flags_g & LOCK_IS_OPEN) && currentChar_g != 0 ) {
				eeprom_update_byte((void*)EEPROM_PASSWORD_LEN, currentChar_g );
				eeprom_update_block(newPassword, (void*) EEPROM_PASSWORD, currentChar_g);
				free(newPassword);
				newPassword = NULL;
				currentChar_g = 0;
				greenLedOff();
				_delay_ms(200);
				greenLedOn();
				_delay_ms(200);
				greenLedOff();		
				_delay_ms(200);
				greenLedOn();	
				_delay_ms(200);

				greenLedOff();
				redLedOn();

				closeLock();
				flags_g &= ~UPDATING_PASSWORD;

				return;
			}

			if (! (flags_g & LOCK_IS_OPEN)) {
				openLock();
				// flags_g |= LOCK_IS_OPEN;
				greenLedOn();
				redLedOff();
				currentChar_g = 0;

				_delay_ms(200);
			}

			break;

		default:
			if ( !(flags_g & UPDATING_PASSWORD) ) {
				char passwordKey = eeprom_read_byte((uint8_t*)EEPROM_PASSWORD + currentChar_g);
				if ( key != passwordKey || (flags_g & LOCK_IS_OPEN)) {
					currentChar_g = 0;
					redLedOn();
					greenLedOff();
					closeLock();
					return;
				} else {
					currentChar_g++;
					// redLedOff();
					if (currentChar_g == passwordLength()) {
						openLock();
						// flags_g |= LOCK_IS_OPEN;
						greenLedOn();
						redLedOff();
						// currentChar_g = 0;
					} else {
						// _delay_ms(30);
						redLedOn();
						greenLedOff();
                        // greenLedOff();
					}
				}
			} else if (newPassword != NULL && currentChar_g < MAX_PASSWORD_LEN) {
					newPassword[currentChar_g] = key;
					currentChar_g++;
					greenLedOff();
					_delay_ms(200);
					greenLedOn();
			}
	}
}

inline char keyToChar(uint8_t key) {
	// for (uint8_t i = 0; i <= key; i++) {
	// 	redLedOff();
	// 	_delay_ms(200);
	// 	redLedOn();
	// 	_delay_ms(200);
	// }
	return eeprom_read_byte((uint8_t*)EEPROM_CHARS + key);
}

inline uint8_t passwordLength() {
	return eeprom_read_byte((uint8_t*)EEPROM_PASSWORD_LEN);
}

inline void greenLedOn() {
	LED_PORT |= (1 << GREEN_LED);
}

inline void greenLedOff() {
	LED_PORT &= ~(1 << GREEN_LED);
}

inline void redLedOn() {
	LED_PORT |= (1 << RED_LED);
}

inline void redLedOff() {
	LED_PORT &= ~(1 << RED_LED);
}

void openLock() {
	LED_PORT |= (1 << LOCK);
	flags_g |= LOCK_IS_OPEN;
}

void closeLock() {
	LED_PORT &= ~(1 << LOCK);
	flags_g &= ~LOCK_IS_OPEN;
}

void initEEPROM() {
	uint16_t header = 0x1488;

	uint16_t test = eeprom_read_word(0);

	if ( header != test ) {
		eeprom_write_word(0, header);
		char list[] = "123456789*#0";
		eeprom_write_block(list , (void*)EEPROM_CHARS, 12 );
		char defaultPassword[] = "91402";
		uint8_t passwordLength = 5;
		eeprom_write_byte((void*)EEPROM_PASSWORD_LEN, passwordLength );
		eeprom_write_block( defaultPassword, (void*)EEPROM_PASSWORD, passwordLength );
	}
}

uint8_t getKeyPressed() {
	KEYPAD_PORT &= ~(0x0F); // disable pull-up on rows
	KEYPAD_PORT |= 0x70; // set column outputs to 1

	for ( uint8_t c = 0; c < 3; c++ ) {
		KEYPAD_DDR &= ~(0x7F); // set all DDRs to 0
		KEYPAD_DDR |= (0x40 >> c); // set one column ddr to 1
		_delay_us(100);
		for ( uint8_t r = 0; r < 4; r++ ) {
			//if ( !( KEYPAD_PIN & (0x08 >> r) ) ) {
			if ( ( KEYPAD_PIN & (0x08 >> r) ) ) {
				return (r*3 + c);
			}
		}
	}

	return 0xFF;
}
