build: lock.hex

lock.o: lock.c
	avr-gcc -g -mmcu=attiny2313 -Wall -Os -c lock.c

lock.elf: lock.o
	avr-gcc -g -mmcu=attiny2313 -Wall -o lock.elf lock.o

lock.hex: lock.elf
	avr-objcopy -j .text -j .data -O ihex lock.elf lock.hex

.PHONY: clean

clean:
	rm *.o *.hex *.elf
